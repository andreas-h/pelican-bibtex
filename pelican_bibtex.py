"""
Pelican BibTeX
==============

A Pelican plugin that populates the context with a list of formatted
citations, loaded from a BibTeX file at a configurable path.

The use case for now is to generate a ``Publications'' page for academic
websites.
"""
# Author: Vlad Niculae <vlad@vene.ro>
# Unlicense (see UNLICENSE for details)

import os

import logging
logger = logging.getLogger(__name__)  # noqa

from pelican import signals

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
try:
    from pybtex.database.input.bibtex import Parser
    from pybtex.database.output.bibtex import Writer
    from pybtex.database import BibliographyData, PybtexError
    from pybtex.backends import html
    from pybtex.style.formatting import plain  # TODO take this from config
except ImportError:
    logger.warn('`pelican_bibtex:` Failed to load dependency `pybtex`')


__version__ = '0.4'


def init_bib(generator):
    # initialisation of the bibliography dictionary
    bib_dict = {}
    generator.context['bibtex'] = bib_dict


def get_bibtex_paths(generator, metadata):
    bibtex = metadata.get('bibtex')
    if not bibtex:
        return {}

    entries = [entry.strip() for entry in bibtex.split(':')[1:]]

    # check for unique keys
    keys = [e.split()[0] for e in entries]
    if len(keys) > len(list(set(keys))):
        logger.warn('Non-unique bibtex collection keys in file `{}`'.format(
                metadata['slug']))
    entries = {e.split()[0]: e.split()[1] for e in entries}

    # check if files exist
    path = os.path.split(os.path.join(generator.context['PATH'],
                                      metadata['slug']))[0]
    _keys_to_delete = []
    for key in entries.keys():
        bibtexpath = os.path.join(path, entries[key])
        if os.path.isfile(bibtexpath):
            entries[key] = bibtexpath
        else:
            logger.warn('Bibtex file `{}` does not exist'.format(bibtexpath))
            _keys_to_delete.append(key)
    for key in _keys_to_delete:
        del(entries[key])
    return entries


def process_bib(bibfile_path):
    # parse BibTeX file
    try:
        bibdata_all = Parser().parse_file(bibfile_path)
    except PybtexError as e:
        logger.warn('`pelican_bibtex:` Failed to parse file {}: {}'
                    ''.format(bibfile_path, str(e)))
        return

    publications = []  # this list will hold the results
    style = plain.Style()  # TODO: select style from configuration
    html_backend = html.Backend()  # TODO: support other backends?
    all_entries = bibdata_all.entries.values()

    # TODO: process entries: extract field info, delete fields, -> callback
    def preprocess_entry(entry): return entry
    #   if 'doi' in entry.fields.keys() and 'url' in entry.fields.keys():
    #       entry.fields['url'] = ''
    #       entry.fields._dict['url'] = ''
    #   entry.fields['doi'] = ''
    #   entry.fields._dict['doi'] = ''
    #   return entry

    all_entries = [preprocess_entry(e) for e in all_entries]

    formatted_entries = style.format_entries(all_entries)

    # TODO: post-process generated text -> callback
    def postprocess_entry(entry): return entry
    formatted_entries = [postprocess_entry(e) for e in formatted_entries]

    for formatted_entry in formatted_entries:
        key = formatted_entry.key
        entry = bibdata_all.entries[key]

        # TODO: extract metadata -> callback
        entry_dict = {}
        entry_dict['year'] = entry.fields.get('year')
        entry_dict['doi'] = entry.fields.get('doi')

        # render the bibtex string for the entry
        bib_buf = StringIO()
        bibdata_this = BibliographyData(entries={key: entry})
        Writer().write_stream(bibdata_this, bib_buf)
        entry_dict['bibtex'] = bib_buf.getvalue()

        # render HTML representation
        text = formatted_entry.text.render(html_backend)
        entry_dict['text'] = text.replace('}', '').replace('{', '')

        entry_dict['key'] = key

        publications.append(entry_dict)

    return publications


def add_bib(generator, metadata):
    bibfiles = get_bibtex_paths(generator, metadata)
    if not bibfiles:
        return
    bib = {key: process_bib(path) for key, path in bibfiles.items()}
    generator.context['bibtex'][metadata['slug']] = bib


def register():
    signals.page_generator_init.connect(init_bib)
    signals.page_generator_context.connect(add_bib)
